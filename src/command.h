#ifndef SWAGSHELL_COMMAND_H
#define SWAGSHELL_COMMAND_H

#include <bits/types/FILE.h>

// todo: is this actually necessary? will we have more flags than 1?
typedef enum {
    COMMAND_BACKGROUND, // for example `sleep 1 &`
} command_flags;

typedef struct command_t {
    const char *command;        // what we actually gotta exec
    struct command_t *pipe_to;  // for pipe
    FILE *outstream;            // for redirect
    command_flags flags;
} command_t;

void cmd_run(char *command);

void cmd_parse(char *commmand, command_t *dest);

#endif
