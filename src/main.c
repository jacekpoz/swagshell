#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "command.h"
#include "swagshell.h"
#include "util.h"

static void usage(char *prog) {
    printf("usage: %s\n", prog);
    printf("       %s SCRIPT-FILE\n", prog);
    printf("       %s -c COMMAND\n", prog);
    exit(EXIT_SUCCESS);
}

int main(int argc, char **argv) {
    if(argc == 1) {
        interactive();
    } else if(argc == 2) {
        // run a script
        // char *script_path = argv[1];

        // for line in file: run_command
        // ig

        NOT_IMPLEMENTED;

    } else if(argc == 3 && strcmp(argv[1], "-c") == 0) {
        // run a single command
        char *command = argv[2];
        cmd_run(command);

    } else {
        usage(argv[0]);
    }

    return EXIT_SUCCESS;
}
