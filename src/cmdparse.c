#include "command.h"
#include "util.h"

// https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html#tag_02_10_2
// scroll down a bit and there you will find this in bison syntax
typedef enum {
    TOKEN_UNKNOWN = 0,

    // generic tokens
    // todo: find out what these are
    TOKEN_WORD,            // ??
    TOKEN_ASSIGNMENT_WORD, // ??
    TOKEN_NAME,            // ??
    TOKEN_NEWLINE,         // \n MOST LIKELY
    TOKEN_IO_NUMBER,       // ??

    // operators or something
    TOKEN_AND_IF,          // &&
    TOKEN_OR_IF,           // ||
    TOKEN_DSEMI,           // ;;

    TOKEN_DLESS,           // <<
    TOKEN_DGREAT,          // >>
    TOKEN_LESSAND,         // <&
    TOKEN_GREATAND,        // >&
    TOKEN_LESSGREAT,       // <>
    TOKEN_DLESSDASH,       // <--

    TOKEN_CLOBBER,         // >|

    TOKEN_LBRACE,          // {
    TOKEN_RBRACE,          // }
    TOKEN_BANG,            // !

    // keywords
    TOKEN_KW_IF,
    TOKEN_KW_THEN,
    TOKEN_KW_ELSE,
    TOKEN_KW_ELIF,
    TOKEN_KW_FI,
    TOKEN_KW_DO,
    TOKEN_KW_DONE,
    TOKEN_KW_CASE,
    TOKEN_KW_ESAC,
    TOKEN_KW_WHILE,
    TOKEN_KW_UNTIL,
    TOKEN_KW_FOR,
    TOKEN_KW_IN,

} token_type_t;


void cmd_parse(char *commmand, command_t *dest) {
    (void) commmand; // avoid unused errors for now
    (void) dest;

    // ls . -lh | xargs echo > out.txt &
    // we receive a command_t with:
    //   command = "ls . -lh"
    //   pipe_to = { .command = "xargs echo", .outstream = file "out.txt" }
    //   flags = COMMAND_BACKGROUND

    // something to keep in mind maybe
    // `ls . -lh | xargs echo > out.txt &` == `(ls . -lh | xargs echo > out.txt) &`
    // `... | (xargs echo > out.txt &)` works in bash, doesnt say anynthing about background processes

    NOT_IMPLEMENTED;
}

